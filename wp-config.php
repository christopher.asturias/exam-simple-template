<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'exam_sample_template' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#W^hn>i4>95b%0Q8OHa|wq9Q:abv>}_+uR2=zv^y[-[g4;f/3.HePO?JAYdenW|3' );
define( 'SECURE_AUTH_KEY',  '#h,nhQV,xw.l~Ms5><VSDV!!*nt$;jK^n&>nstl2Lt(.?)mXWv.JRJTew,C9^/I>' );
define( 'LOGGED_IN_KEY',    'E5c]iFg$#%*,!!p/pCJiV2#|yfZG=15|;XH;D@Y&x%@KHm|IByUa,&[V5Zx<**Q!' );
define( 'NONCE_KEY',        'dr1DQ}CPgDAXCH[n7,va,1{)M{<RdNIOw+`kYbnKmS;0wj%pFQ~F*U^*,yJmr6R#' );
define( 'AUTH_SALT',        '[lY1o mq>:$gjTAJzM{JX8]`3 }TsY-QH#ixcMxtOawMR`:u%(|wce9mb`@=yO~+' );
define( 'SECURE_AUTH_SALT', '`PF]wYhLCw,Lb0,@79lt^6-^nX[=Um2t9?/&`vkmQuqM9B+Fc_L?Ba/zeqS*=a^x' );
define( 'LOGGED_IN_SALT',   '}+u?$jS=3S%cQQU3j zKBDA{j?Z#4 vg !&LF,86/F5SMK{W;WnT|?BsJ.JW3cTc' );
define( 'NONCE_SALT',       'LTOg}~Q4%Y*ODboy:416jNG%/~~q2,/)?V@^XI2@5kY??g4:4Zo =48)]52Vz`UO' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
